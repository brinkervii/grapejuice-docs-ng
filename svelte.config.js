import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';
import { mdsvex } from 'mdsvex';
import { resolve } from "path";

const markdownLayout = resolve("./src/lib/components/Markdown.svelte");
/** @type {import('@sveltejs/kit').Config} */
const config = {
    preprocess: [
        vitePreprocess(),
        mdsvex({
            layout: markdownLayout,
            extensions: ['.md', '.sxv'],
        })
    ],

    extensions: [
        '.svelte', '.svx', '.md'
    ],

    kit: {
        adapter: adapter()
    }
};

export default config;
